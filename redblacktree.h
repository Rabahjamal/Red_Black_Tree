#ifndef REDBLACKTREE_H
#define REDBLACKTREE_H

#include <iostream>
#include <memory>
#include <vector>

using std::unique_ptr;
using std::forward;
using std::make_unique;
enum Color
{
	Red,
	Black
};
template <typename T>
class Node
{
public:
	T data;
	Color color; // 1 for red, 0 for black
	//    Node<T>* left;
	//    Node<T>* right;
	Node* parent;

	unique_ptr<Node> right;
	unique_ptr<Node> left;
	Node(T&& data, Color color) : data(forward<T>(data)), color(color),
								  left(nullptr), right(nullptr), parent(nullptr) {}

};
template <typename T>
class RedBlackTree
{
private:

public:
	unique_ptr<Node<T>> root;
	size_t size;
    std::vector<T> tree_traversed;

	Node<T>* grandparent(const Node<T>*);
	Node<T>* uncle(const Node<T>*);
	Node<T>* sibling(const Node<T>*);
	void traverse_the_tree(const unique_ptr<Node<T>>&, std::vector<T>&);
	Node<T>* find(const T&);
	Node<T>* get_max_node(const Node<T>*);
//	void replace_node()
//	void insert(const T&, const unique_ptr<Node<T>>&);
//	bool search(const T&, const unique_ptr<Node<T>>&);

    RedBlackTree();
    //~RedBlackTree();
	bool search(const T&);
	void insert(T&&);
	void remove(const T&);
	size_t get_children_count(const Node<T>*) const;
    std::vector<T> traverse();

};

#endif // REDBLACKTREE_H

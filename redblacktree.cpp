#include "redblacktree.h"

template <typename T>
RedBlackTree<T>::RedBlackTree() : root(nullptr), size(0)
{
}

template <class T>
bool RedBlackTree<T>::search(const T& value)
{
	auto tmp = root.get();
	while (tmp != nullptr)
	{
		if (value < tmp->data)
		{
			tmp = tmp->left.get();
		}
		else if (value > tmp->data)
		{
			tmp = tmp->right.get();
		}
		else
		{
			return true;
		}
	}
	return false;
}

template <class T>
void RedBlackTree<T>::insert(T&& value)
{
	this->size++;
	if (root == nullptr)
	{
		root = make_unique<Node<T>>(forward<T>(value), Color::Black);
		return;
	}

	Node<T>* tmp = root.get();

	while (tmp != nullptr)
	{

		if (value < tmp->data)
		{

			auto l = tmp->left.get();
			if (l == nullptr)
			{
				tmp->left = make_unique<Node<T>>(forward<T>(value), Color::Red);
				;
				tmp->left->parent = tmp;
				return;
			}
			tmp = l;
		}
		else if (value > tmp->data)
		{
			auto r = tmp->right.get();
			if (r == nullptr)
			{
				tmp->right = make_unique<Node<T>>(forward<T>(value), Color::Red);
				tmp->right->parent = tmp;
				return;
			}
			tmp = r;
		}
		else
		{
			return;
		}
	}
}

template <class T>
std::vector<T> RedBlackTree<T>::traverse()
{
	std::vector<T> vec;
    traverse_the_tree(root, vec);
    return vec;
}

template <class T>
void RedBlackTree<T>::traverse_the_tree(const unique_ptr<Node<T>>& node, std::vector<T>& vec)
{
	if (node == nullptr)
    {
        return;
    }
    traverse_the_tree(node->left, vec);
    vec.push_back(node->data);
    traverse_the_tree(node->right, vec);
}
template <typename T>
size_t RedBlackTree<T>::get_children_count(const Node<T>* node) const
{
	return node->left != nullptr + node->right != nullptr;
}
template <typename T>
Node<T>* RedBlackTree<T>::get_max_node(const Node<T>* node)
{
	auto tmp = node;
	while(tmp->right !=nullptr)
	{
		tmp = tmp->right;
	}
	return tmp;
}


template <typename T>
void RedBlackTree<T>::remove(const T& value)
{
	Node<T>* n = find(value);
	if (n == nullptr)
	{
		return;
	}
	if (get_children_count(n) == 2)
	{
		Node<T>* pred = get_max_node(n->left);
		n->value =	pred->value;
		n = pred;
	}

}
template <typename T>
Node<T>* RedBlackTree<T>::find(const T& value)
{
	auto tmp = root.get();
	while (tmp != nullptr)
	{
		if (value < tmp->data)
		{
			tmp = tmp->left.get();
		}
		else if (value > tmp->data)
		{
			tmp = tmp->right.get();
		}
		else
		{
			return tmp;
		}
	}
	return nullptr;
}
template <typename T>
Node<T>* RedBlackTree<T>::grandparent(const Node<T>* node)
{

	if (node->parent == nullptr)
	{
		return nullptr;
	}
	return node->parent->parent;
}

template <typename T>
Node<T>* RedBlackTree<T>::sibling(const Node<T>* node)
{
	const auto p = node->parent;
	if (node->parent == nullptr)
	{
		return nullptr; // No parent means no sibling
	}
	if (node == p->left.get())
	{
		return p->right.get();
	}
	return p->left.get();
}

template <typename T>
Node<T>* RedBlackTree<T>::uncle(const Node<T>* node)
{
	auto gp = this->grandparent(node);

	if (gp == nullptr)
	{
		return nullptr; // No grandparent means no uncle
	}
	return sibling(node->parent);
}

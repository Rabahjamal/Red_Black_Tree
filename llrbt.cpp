#include "llrbt.h"
#include <queue>
template <typename Key, typename Val>
bool LLRBT<Key, Val>::is_red(unique_ptr<LLRBT::Node>& n)
{
	return n == nullptr ? false : n->c == Color::Red;
}


// unique_ptr<typename LLRBT<Key, Val>::Node>
template <typename Key, typename Val>
void LLRBT<Key, Val>::rotate_left(unique_ptr<LLRBT::Node>& n)
{
    unique_ptr<LLRBT::Node> tmp = std::move(n->right);
    n->right = std::move(tmp->left);
    tmp->left = std::move(n);

	tmp->c = tmp->left->c;
	tmp->left->c = Color::Red;
	n = std::move(tmp);
    // return tmp;
}


template <typename Key, typename Val>
void LLRBT<Key, Val>::rotate_right(unique_ptr<LLRBT::Node>& n)
{
    unique_ptr<LLRBT::Node> tmp = std::move(n->left);
    n->left = std::move(tmp->right);
    tmp->right = std::move(n);
	tmp->c = tmp->right->c;
	tmp->right->c = Color::Red;
	n = std::move(tmp);
}


template <typename Key, typename Val>
void LLRBT<Key, Val>::flip_colors(unique_ptr<LLRBT::Node>& n)
{
    //	if (is_red(n) || !is_red(n.left.get()) || !is_red(n.right.get()))
    //	{
    //		return;
    //	}
    if(n != nullptr && n->left != nullptr && n->right != nullptr)
    {
        if(n->c == n->left->c || n->c == n->right->c)
        {
            return;
        }
        if(n->c == Color::Black)
        {
            n->c = Color::Red;
            n->left->c = Color::Black;
            n->right->c = Color::Black;
        }
        else
        {
            n->c = Color::Black;
            n->left->c = Color::Red;
            n->right->c = Color::Red;
        }
    }
}


template <typename Key, typename Val>
void LLRBT<Key, Val>::insert(unique_ptr<LLRBT::Node>& n, const Key& k, const Val& v)
{

	if (n == nullptr)
	{
        sz++;
		n = make_unique<Node>(k, v, Color::Red);
	}
	else if (k < n->k)
	{
		//        insert(n->left, std::forward<Key>(k), std::forward<Val>(v));
		insert(n->left, k, v);
	}
	else if (k > n->k)
	{

		//        insert(n->right, std::forward<Key>(k), std::forward<Val>(v));
		insert(n->right, k, v);
    }
	else
	{
		n->k = k;
	}

	if (is_red(n->right) && !is_red(n->left))
	{
		rotate_left(n);
    }
	if (n->left)
	{
		if (is_red(n->left) && is_red(n->left->left))
		{

			rotate_right(n);
		}
    }
	if (is_red(n->left) && is_red(n->right))
	{

        flip_colors(n);
	}
}


template <typename Key, typename Val>
void LLRBT<Key, Val>::insert(const Key& k, const Val& v)
{
	if (root == nullptr)
	{
		root = make_unique<Node>(k, v, Color::Black);
		sz++;
		return;
	}
	insert(root, k, v);
    root->c = Color::Black;
}


template <typename Key, typename Val>
Val& LLRBT<Key, Val>::find(const Key& k)
{
	auto tmp = root.get();
	while (tmp != nullptr)
	{
		if (k < tmp->k)
		{
			tmp = tmp->left.get();
		}
		else if (k > tmp->k)
		{
			tmp = tmp->right.get();
		}
		else
		{
			return tmp->v;
		}
	}
//	delete tmp;
//	return nullptr;
}

template <typename Key, typename Val>
void LLRBT<Key, Val>::traverse_inorder(unique_ptr<Node>& n)
{
	if (n == nullptr)
	{
		return;
	}
	traverse_inorder(n->left);
	std::cout << n->k << " - " << n->v << " ";
	if (n->k == root->k)
		std::cout << "Root ";
	std::cout << ((n->c == 0) ? "Red\n" : "Black\n");
	traverse_inorder(n->right);
}
template <typename Key, typename Val>
void LLRBT<Key, Val>::traverse_preorder(unique_ptr<Node>& n)
{
	if (n == nullptr)
	{
		return;
	}
	std::cout << n->k << " - " << n->v << " ";
	if (n->k == root->k)
		std::cout << "Root ";
	std::cout << ((n->c == 0) ? "Red\n" : "Black\n");

	traverse_inorder(n->left);

	traverse_inorder(n->right);
}

template <typename Key, typename Val>
void LLRBT<Key, Val>::traverse_level(unique_ptr<LLRBT::Node>& n)
{
	if (n == nullptr)
	{
		return;
	}
	std::queue<Node*> q;
	q.push(root.get());
	while (true)
	{
		int nodeCount = q.size();
		if (nodeCount == 0)
			break;
		while (nodeCount > 0)
		{
			Node* node = q.front();
            std::cout << "( " << node->k << ", " << node->c << ") ";
			q.pop();
			if (node->left != nullptr)
				q.push(node->left.get());
			if (node->right != nullptr)
				q.push(node->right.get());
			nodeCount--;
		}
		std::cout << std::endl;
	}
}
//template <typename Key, typename Val>
//void LLRBT<Key, Val>::insert(Key&& k, Val&& v)
//{
//	if (root == nullptr)
//	{
//		root = make_unique<Node>(std::forward<Key>(k), std::forward<Val>(v), Color::Black);
//		sz++;
//		return;
//	}
//	insert(root, std::forward<Key>(k), std::forward<Val>(v));
//	// root->c = Color::Black;
//}

template <typename Key, typename Val>
void LLRBT<Key, Val>::traverse(const std::string& mode)
{
	if (mode == "inorder")
	{
		traverse_inorder(root);
	}
	else if (mode == "preorder")
	{
		traverse_preorder(root);
	}
	else if (mode == "level")
	{
		traverse_level(root);
	}
}


/***********************************************************/
template <typename Key, typename Val>
void LLRBT<Key, Val>::fixUp(unique_ptr<Node>& h)
{
    if(h == nullptr)
    {
        return;
    }

    if(is_red(h->right))
    {
        rotate_left(h);
    }
    if(h->left != nullptr && is_red(h->left) && is_red(h->left->left))
    {
        rotate_right(h);
    }
    if (is_red(h->left) && is_red(h->right))
    {
        flip_colors(h);
    }
}


template <typename Key, typename Val>
void LLRBT<Key, Val>::move_red_right(unique_ptr<Node>& h)
{
    if(h==nullptr || h->right == nullptr || h->left == nullptr)
    {
        return;
    }
    if(!is_red(h) && is_red(h->right) && is_red(h->right->left))
    {
        return;
    }

    flip_colors(h);
    if(is_red(h->left->left))
    {
        rotate_right(h);
        flip_colors(h);
    }
}


template <typename Key, typename Val>
void LLRBT<Key, Val>::move_red_left(unique_ptr<Node>& h)
{
    if(h==nullptr || h->right == nullptr || h->left == nullptr)
    {
        return;
    }
    if(!is_red(h) && is_red(h->left) && is_red(h->left->left))
    {
        return;
    }


    flip_colors(h);
    if(is_red(h->right->left))
    {
        rotate_right(h->right);
        rotate_left(h);
        flip_colors(h);
    }
}


template <typename Key, typename Val>
void LLRBT<Key, Val>::deleteMax()
{

    if (root == nullptr)
        return;
    if (!is_red(root->left) && !is_red(root->right))
    {
        root->c = Color::Red;
    }

    deleteMax(root);

    if (sz)
        root->c = Color::Black;

}


template <typename Key, typename Val>
void LLRBT<Key, Val>::deleteMax(unique_ptr<Node>& h)
{
    if(is_red(h->left))
        rotate_right(h);

    if(h->right == nullptr)
    {
        sz--;
        h.reset(nullptr);
        return;
    }
    if(!is_red(h->right) && !is_red(h->right->left))
    {
        move_red_right(h);
    }
    deleteMax(h->right);

    fixUp(h);
}



template <typename Key, typename Val>
void LLRBT<Key, Val>::deleteMin()
{
    if(!is_red(root->left) && !is_red(root->right))
    {
        root->c = Red;
    }

    deleteMin(root);
    if(sz)
        root->c = Black;
}



template <typename Key, typename Val>
void LLRBT<Key, Val>::deleteMin(unique_ptr<Node>& h)
{
    if(h->left == nullptr)
    {
        h.reset(nullptr);
        sz--;
        return;
    }

    if(!is_red(h->left) && !is_red(h->left->left))
        move_red_left(h);


    deleteMin(h->left);
    fixUp(h);
}



template <typename Key, typename Val>
void LLRBT<Key, Val>::remove(Key key)
{
    if(!is_red(root->left) && !is_red(root->right))
    {
        root->c = Color::Red;
    }

    remove(root, key);
    if(sz)
    {
        root->c = Color::Black;
    }
}

template <typename Key, typename Val>
void LLRBT<Key, Val>::remove(unique_ptr<Node>& h, Key key)
{
    if(key < h->k)
    {
        if(!is_red(h->left) && !is_red(h->left->left))
        {
            move_red_left(h);
        }
        remove(h->left, key);
    }
    else
    {
        if(is_red(h->left))
        {
            rotate_right(h);
        }
        if(key == h->k && h->right == nullptr)
        {
            h.reset(nullptr);
            sz--;
            return;
        }
        if(!is_red(h->right) && !is_red(h->right->left))
        {
            move_red_right(h);
        }
        if(key == h->k)
        {

            std::pair<Key, Val> x = get_min(h->right.get());
            h->k = x.first;
            h->v = x.second;

            //std::cout << x.first << " " << x.second << std::endl;
            deleteMin(h->right);
        }
        else
        {
            remove(h->right, key);
        }
    }
    fixUp(h);
}


template <typename Key, typename Val>
std::pair<Key, Val> LLRBT<Key, Val>::get_min(Node* x)
{
    //std::cout << x->k << " " << x->v << std::endl;
    if(x->left == nullptr)
    {
        return std::make_pair(x->k, x->v);
    }
    else
    {
        return get_min(x->left.get());
    }
}





//#define CATCH_CONFIG_MAIN
//#include "catch.hpp"
//#include "redblacktree.cpp"
#include "llrbt.cpp"
#include <chrono>
#include <iostream>
#include <map>
#include <vector>
using namespace std;

void println()
{
	std::cout << "--------------------------------------------------\n";
}
int main()
{

    /*auto start = std::chrono::steady_clock::now();
	// do something

	std::string mode = "level";
	int x = 1;
	LLRBT<int, int> t;
	for (int i = 0; i < 10000; ++i)
	{
		t.insert(i, i);
	}
	for (int i = 0; i < 10000; ++i)
	{
		auto res = t.find(i);
//		std::cout<<res<<" ";
	}
	auto finish = std::chrono::steady_clock::now();
	double elapsed_seconds = std::chrono::duration_cast<std::chrono::duration<double>>(finish - start).count();
	std::cout << elapsed_seconds << std::endl;

	start = std::chrono::steady_clock::now();
	std::map<int, int> m;
	auto u = m[4];
	for (int i = 0; i < 10000; ++i)
	{
		m.insert({ i, i });
	}
	finish = std::chrono::steady_clock::now();
	elapsed_seconds = std::chrono::duration_cast<std::chrono::duration<double>>(finish - start).count();
	std::cout << elapsed_seconds << std::endl;

	LLRBT_iterator<int,int> it(t.root.get());
	auto i =0;
	while(i++<100)
	{
		auto i = it++;
		std::cout<<i.first<<" "<<i.second<<endl;
	}
//	t.traverse(mode);
	//	println();
	//	t.insert(6, 3);
	//	t.traverse(mode);
	//	println();
	//	t.insert(20, 3);
	//	t.traverse(mode);
	//	println();
	//	t.insert(21, 3);
	//	t.traverse(mode);
	//	println();
	//	t.insert(23, 3);
	//	t.traverse(mode);
	//	println();
	//	t.insert(26, 3);
	//	t.traverse(mode);
	//	println();
	//	t.insert(10, 1);
	//	t.insert(-1, 3);
	//	t.insert(9, 1);
	//	t.insert(11, 3);

	//	std::cout << t.root->k << " :: " << t.root->v << std::endl;
	//	std::cout << t.root->left->k << " :: " << t.root->left->v << std::endl;
	//	RedBlackTree<int> rbt;

	//	rbt.insert(12);
	//	rbt.insert(7);
	//	rbt.insert(15);
	//	rbt.insert(13);
	//	rbt.insert(19);
	//	rbt.insert(17);
	//	rbt.insert(21);
	//	rbt.insert(3);
	//	rbt.insert(8);
	//	rbt.insert(16);
	//	//	std::cout << rbt.search(4) << std::endl;
	//	//	std::cout << rbt.search(5) << std::endl;
	//	//	std::cout << rbt.search(6) << std::endl;
	//	//	std::cout << rbt.search(4) << std::endl;
	//	//	std::cout << rbt.search(4) << std::endl;
	//	//	std::cout << rbt.search(10) << std::endl;
	//	//	std::cout << rbt.search(3) << std::endl;
	//	auto rt = rbt.grandparent(rbt.root->left->left.get());
	//	auto un = rbt.uncle(rbt.root->left->left.get());

	//	std::cout << rt->data << std::endl;
	//	std::cout << un->data << std::endl;
	//    rbt.traverse(root);
	//    cout << endl;
	//    Node<int>* n = rbt.search(16);

	//    cout << n->parent->data << endl;
	//    cout << rbt.get_grandparent(n)->data << endl;
	//    //cout << n->parent->data << endl;
    //    cout << rbt.get_uncle(n)->data << endl;*/


    LLRBT<int, int> l;

//    l.insert('H', '-');
//    l.insert('D', '-');
//    l.insert('L', '-');
//    l.insert('B', '-');
//    l.insert('F', '-');
//    l.insert('J', '-');
//    l.insert('N', '-');
//    l.insert('A', '-');
//    l.insert('C', '-');
//    l.insert('E', '-');
//    l.insert('G', '-');
//    l.insert('I', '-');
//    l.insert('K', '-');
//    l.insert('M', '-');
    //l.insert('O', '-');

    //l.insert('B', '-');
//    l.insert('M', '-');
//    l.insert('E', '-');
//    l.insert('R', '-');
//    l.insert('C', '-');
//    l.insert('H', '-');
//    l.insert('P', '-');
//    l.insert('X', '-');
//    l.insert('A', '-');
//    l.insert('S', '-');

    l.insert(3, 0);
    l.insert(1, 0);
    l.insert(8, 0);
    l.insert(0, 0);
    l.insert(2, 0);
    l.insert(5, 0);
    l.insert(9, 0);
    l.insert(4, 0);
    l.insert(6, 0);


    l.traverse("level");
    println();
    l.remove(1);

    l.traverse("level");

    l.traverse("level");
    println();
    l.deleteMax();
    l.traverse("level");
    println();
    l.deleteMax();
    l.traverse("level");
    println();
    l.deleteMax();
    l.traverse("level");
    println();
    l.deleteMax();
    l.traverse("level");
    println();
    l.deleteMax();
    l.traverse("level");
    println();
    l.deleteMax();
    l.traverse("level");
    println();
    l.deleteMax();
    l.traverse("level");
    println();
    l.deleteMax();
    l.traverse("level");
    println();
    l.deleteMax(); l.traverse("level");
    println();
    l.deleteMax();



	return 0;
}

//test case 1
//TEST_CASE("Insert and search in Red Black Tree")
//{
//	RedBlackTree<int> rbt;

//	rbt.insert(12);
//	rbt.insert(7);
//	rbt.insert(15);
//	rbt.insert(13);
//	rbt.insert(19);
//	rbt.insert(17);
//	rbt.insert(21);
//	rbt.insert(3);
//	rbt.insert(8);
//	rbt.insert(16);

//	REQUIRE(rbt.search(12) == true);
//	REQUIRE(rbt.search(17) == true);
//	REQUIRE(rbt.search(19) == true);
//	REQUIRE(rbt.search(15) == true);
//	REQUIRE(rbt.search(30) == false);
//	REQUIRE(rbt.search(177) == false);
//	REQUIRE(rbt.search(190) == false);
//	REQUIRE(rbt.search(1) == false);
//}

////test case 2
//TEST_CASE("Check order of the tree")
//{
//	RedBlackTree<int> rbt;

//	rbt.insert(12);rbt.insert(7);rbt.insert(15);rbt.insert(13);rbt.insert(19);
//	rbt.insert(17);rbt.insert(21);rbt.insert(3);rbt.insert(8);rbt.insert(16);

//	std::vector<int> test_1 = rbt.traverse();
//	std::vector<int> correct_1{3, 7, 8, 12, 13, 15, 16, 17, 19, 21};

//	REQUIRE(test_1 == correct_1);

//	RedBlackTree<int> rbt_2;

//	rbt_2.insert(7); rbt_2.insert(4); rbt_2.insert(12); rbt_2.insert(3);
//	rbt_2.insert(3); rbt_2.insert(5); rbt_2.insert(10); rbt_2.insert(18);
//	rbt_2.insert(16);rbt_2.insert(23);

//	std::vector<int> test_2 = rbt_2.traverse();
//	std::vector<int> correct_2{3, 4, 5, 7, 10, 12, 16, 18, 23};

//	REQUIRE(test_2 == correct_2);
//}

//TEST_CASE("Relationship")
//{
//	RedBlackTree<int> rbt;

//	rbt.insert(12); rbt.insert(7); rbt.insert(15); rbt.insert(13); rbt.insert(19);
//	rbt.insert(17); rbt.insert(21); rbt.insert(3); rbt.insert(8); rbt.insert(16);

//	REQUIRE(rbt.search(16)->parent->data == 17);
//	REQUIRE(rbt.search(13)->parent->data == 15);
//	REQUIRE(rbt.search(3)->parent->data == 7);
//	REQUIRE(rbt.get_grandparent(rbt.search(8))->data == 12);
//	REQUIRE(rbt.get_grandparent(rbt.search(21))->data == 15);
//	REQUIRE(rbt.get_grandparent(rbt.search(19))->data == 12);
//	REQUIRE(rbt.get_uncle(rbt.search(3))->data == 15);
//	REQUIRE(rbt.get_uncle(rbt.search(13))->data == 7);
//	REQUIRE(rbt.get_uncle(rbt.search(17))->data == 13);

//}

#ifndef LLRBT_H
#define LLRBT_H
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <queue>
enum Color
{
	Red,
	Black
};
using std::unique_ptr;
using std::make_unique;

template <typename Key, typename Val>
class LLRBT; // incomplete declration

template <typename Key, typename Val>
class LLRBT_iterator; // incomplete declration

template <typename Key, typename Val>
class LLRBT
{
private:
	class Node
	{
	public:
		Key k;
		Val v;
		unique_ptr<Node> left, right;
		Color c;
		Node(const Key& k, const Val& v, const Color& c) : k(k), v(v), c(c),
														   left(nullptr),
														   right(nullptr) {}
	};

	size_t sz{ 0 };
    std::pair<Key, Val> get_min(Node*);
    void fixUp(unique_ptr<Node>&);
    void move_red_right(unique_ptr<Node>&);
    void move_red_left(unique_ptr<Node>&);
    void deleteMin(unique_ptr<Node>&);
    void deleteMax(unique_ptr<Node>&);
    void remove(unique_ptr<Node>&, Key key);
	bool is_red(unique_ptr<Node>&);
	void rotate_left(unique_ptr<Node>&);
	void rotate_right(unique_ptr<Node>&);
	void flip_colors(unique_ptr<Node>&);
	void insert(unique_ptr<Node>&, const Key& k, const Val& v);

	void traverse_inorder(unique_ptr<Node>&);
	void traverse_preorder(unique_ptr<Node>&);
	void traverse_level(unique_ptr<Node>&);
	friend class LLRBT_iterator<Key, Val>;

public:
	using const_iterator = LLRBT_iterator<Key, Val>;
	unique_ptr<Node> root;
	LLRBT() : sz(0), root(nullptr){};

	void insert(const Key& k, const Val& v);
	Val& find(const Key& k);
	void traverse(const std::string& mode);
    void deleteMin();
    void deleteMax();
    void remove(Key);

};

template <typename Key, typename Val>
class LLRBT_iterator
{
private:
	using self_type = LLRBT_iterator;
	using value_type = std::pair<Key, Val>;
	using refrence = value_type&;
	using const_refrence = const refrence;
	using pointer = value_type*;

	//	typedef ptrdiff_t difference_type;
	std::vector<value_type> v;
	typename LLRBT<Key, Val>::Node* n;
	size_t index;

public:
	LLRBT_iterator(typename LLRBT<Key, Val>::Node* node) : index(0)
	{
		in_order(node);
	}
	void in_order(typename LLRBT<Key, Val>::Node* node)
	{
		if (node == nullptr)
		{
			return;
		}
		in_order(node->left.get());
		v.push_back({ node->k, node->v });
		in_order(node->right.get());
	}
	refrence operator++()
	{
		if (index > v.size())
			return nullptr;
		return v[index++];
	}
	value_type operator++(int)
	{
		if (index > v.size())
			index = 0;
		return v[index++];
	}
};
#endif // LLRBT_H
